package id.dwikiriyadi.todo.ui.task

import androidx.lifecycle.ViewModel
import id.dwikiriyadi.todo.data.model.ToDo
import id.dwikiriyadi.todo.data.repository.ToDoRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class TaskViewModel(private val toDoRepository: ToDoRepository): ViewModel() {

    private val parentJob = Job()

    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main

    private val scope = CoroutineScope(coroutineContext)

    fun getItem(id: Int) = toDoRepository.getItem(id)

    fun delete(toDo: ToDo) = scope.launch(Dispatchers.IO) { toDoRepository.delete(toDo) }

    fun update(toDo: ToDo) = scope.launch(Dispatchers.IO) { toDoRepository.update(toDo) }
}