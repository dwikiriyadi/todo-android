package id.dwikiriyadi.todo.utility

import android.content.Context
import id.dwikiriyadi.todo.data.AppDatabase
import id.dwikiriyadi.todo.data.repository.ToDoRepository
import id.dwikiriyadi.todo.ui.main.MainViewModelFactory
import id.dwikiriyadi.todo.ui.task.TaskViewModelFactory

object ViewModelInjector {

    private fun getToDoRepository(context: Context): ToDoRepository {
        return ToDoRepository.getInstance(AppDatabase.getInstance(context).todoDao())
    }

    fun provideMainViewModelFactory(context: Context): MainViewModelFactory {
        val repository = getToDoRepository(context)
        return MainViewModelFactory(repository)
    }

    fun provideTaskViewModelFactory(context: Context): TaskViewModelFactory {
        val repository = getToDoRepository(context)
        return TaskViewModelFactory(repository)
    }
}